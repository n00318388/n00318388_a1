﻿<%@ Page Language="C#" Inherits="assign1__N00318388.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Assignment 1</title>
</head>
<body>
	 <form id="form1" runat="server">
        <div>
            <p>Cleaning Service</p>
            <asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:RangeValidator runat="server" ControlToValidate="clientPhone" Type="Integer" minimumvalue="10" maximumvalue="10" errormessage="Please enter Phone Number"></asp:RangeValidator>
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox runat="server" ID="clientAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientAddress" ErrorMessage="Please enter an address"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="cleanTime" placeholder="Appointment Time"></asp:TextBox>
            <br />
            <p>Number of Rooms</p>
            <asp:RadioButton runat="server" Text="2" GroupName="via"/>
            <asp:RadioButton runat="server" Text="3" GroupName="via"/>
            <asp:RadioButton runat="server" Text="4" GroupName="via"/>
            <asp:RadioButton runat="server" Text="5" GroupName="via"/>
            <asp:RadioButton runat="server" Text="6" GroupName="via"/>
            <br />
            <p>Type of Place</p>
            <asp:DropDownList runat="server" ID="place">
                <asp:ListItem Value="S" Text="Apartment"></asp:ListItem>
                <asp:ListItem Value="M" Text="House"></asp:ListItem>
                <asp:ListItem Value="L" Text="Business"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <p>Payment</p>
            <asp:RadioButton runat="server" Text="Paypal" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Credit/Debit" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Check" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Cash" GroupName="via"/>
            <br />  
            <p>Extras</p>
            <div id="extra" runat="server">
            <asp:CheckBox runat="server" ID="extraService1" Text="Laundry" />
            <asp:CheckBox runat="server" ID="extraService2" Text="Dishes" />
            <asp:CheckBox runat="server" ID="extraService3" Text="Yard Work" />
            </div>
            <br />
            <br />
            <div runat="server" ID="res"></div>
            
        </div>
		<asp:Button id="button1" runat="server" Text="Submit!" OnClick="button1Clicked" />
	</form>
</body>
</html>
